<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('Contact-us',function(){
	return view('Contact');
});
Route::get('Labrary',function(){
	return view('Labrary');
});
Route::get('Blog',function(){
	return view('Blog');
});
Route::get('Login',function(){
	return view('Admin.Login');
});
Route::get('Corporate',function(){
	return view('Corporate');
});
Route::get('InHouse',function(){
	return view('inHouse');
});
Route::get('Elearning',function(){
	return view('Elearning');
});
Route::get('Custom',function(){
	return view('Custom');
});
Route::get('Blended',function(){
	return view('Blended');
});
