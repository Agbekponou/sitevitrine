<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Test">
    <meta name="keywords" content="Manup, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Corporate</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">


    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/plugins/themify/css/themify-icons.css')}}">
    <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
 
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container">
            <div class="logo">
                <a href="./index.html">
                 <img src="{{asset('assets/images/')}}">
                </a>
            </div>
            <div class="nav-menu">
                <nav class="mainmenu mobile-menu">
                    <ul>
                        <li class=""><a href="/">Home</a></li>
                        <li class=""><a href="Labrary">Library</a></li>
                        <li class="active"><a href="Corporate">Corporate</a></li>
                        <li class=""><a href="Contact-us">Contact Us</a></li>
                     
                        
                    </ul>
                </nav>
                
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Hero Section Begin -->
    <section class="hero-section set-bg" data-setbg="{{asset('assets/img/hero.jpg')}}">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="block text-center">
                <h1 class="text-white  slide-in-up">Corporate Learning</h1>
                <br>
                <br>
                <br>
                
                <br>
                <ul class="list-inline">
                  <li class="list-inline-item"><a href="index.html" class="text-white">Home</a></li>
                  <li class="list-inline-item"><span class="text-white"></span></li>
                  <li class="list-inline-item"><a href="InHouse" class="text-white-50">In-house Training</a></li>
                  <li class="list-inline-item"><span class="text-white"></span></li>
                  <li class="list-inline-item"><a href="Blended" class="text-white-50">Blended Learning</a></li> <li class="list-inline-item"><span class="text-white"></span></li>
                  <li class="list-inline-item"><a href="Elearning" class="text-white-50">E-Learning</a></li> <li class="list-inline-item"><span class="text-white"></span></li>
                  <li class="list-inline-item"><a href="Custom" class="text-white-50">Custom Content</a></li>
                </ul>
                <br>
                <br><br>
                
              </div>
            </div>
          </div>

            
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Counter Section Begin -->
    
    <!-- Counter Section End -->

    <!-- Home About Section Begin -->
    <br>
    <br>
    <br>
    <section class="about-section spad">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <div class="section-title">
                      <h2 style="text-align: center;"> About</h2>
                      <p class="f-para">There are several ways people can make money online. From selling products to advertising. In this article I am going to explain the concept of contextual advertising.</p>
                      
                      <p>First I will explain what contextual advertising is. Contextual advertising means the advertising of products on a website according to the content the page is displaying. For example if the content of a website was information on a Ford truck then the advertisements would be for Ford trucks for sale, or Ford servicing etc. It picks up the words on the page and displays ads that are similar to those words. Then when someone either performs an action or clicks on your page you will get paid.</p>
                  </div>
              </div>
              <div class="col-md-4 justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center py-4 bg-light mb-5">
                  <div class="text">
                    <div class="d-flex justify-content-center align-items-center">
                      <a href=""><i class="ti-vector icon "></i></a>
                    </div>
                   
                    <h2>Learn</h2>
                  </div>
                </div>
              </div>
              <div class="col-md-4 justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center py-4 bg-light mb-5">
                  <div class="text">
                    <div class="d-flex justify-content-center align-items-center">
                      <span class="flaticon-handshake"><i class="ti-vector"></i></span>
                    </div>

                    <h2> Grow</h2>
                  </div>
                </div>
              </div>
              <div class="col-md-4 justify-content-center counter-wrap ftco-animate ">
                <div class="block-18 text-center py-4 bg-light mb-5">
                  <div class="text">
                    <div class="d-flex justify-content-center align-items-center">
                      <span class="flaticon-chair"><i class="ti-vector"></i></span>
                    </div>
                    <h2>Implement</h2>
                  </div>
                </div>
              </div>
              <div class="col-lg-12">
                
                   
                    <p class="f-para">There are several ways people can make money online. From selling products to advertising. In this article I am going to explain the concept of contextual advertising.</p>
                    
                    <p>First I will explain what contextual advertising is. Contextual advertising means the advertising of products on a website according to the content the page is displaying. For example if the content of a website was information on a Ford truck then the advertisements would be for Ford trucks for sale, or Ford servicing etc. It picks up the words on the page and displays ads that are similar to those words. Then when someone either performs an action or clicks on your page you will get paid.</p>
            
            </div>
          </div>
         
      </div>
  </section>

  <section class="about-section spad color">
      <div class="container">
          
          <div class="row">
              
              <div class="col-lg-12">
                  <div class="section-title">
                      <h2 style="text-align: center;">Request more information</h2>
                     
                      </div>
                      <div class="justify-content-center align-items-center text-center text-margin">
                           
                           <p class="f-para">There are several ways people <br>can make money online. From selling products to advertising. <br> In this article I am going to explain the concept of contextual advertising.</p>
                      </div>

                      <div class="justify-content-center align-items-center text-center">
                           <a href="#" class="btn btn-primary stretched-link">Go somewhere</a>

                      </div>

                    
              </div>

              
          </div>
      </div>
  </section>
    <section class="about-section spad">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <div class="section-title">
                      <h2 style="text-align: center;"> Event types</h2>
                     
                      </div>
              </div>

              <div class="col-lg-3 col-md-6 mb-5">
                <div class="blog-item">
                  <img src="{{asset('assets/images/blog/1.jpg')}}" alt="" class="img-fluid rounded">
              
                  <div class="blog-item-content bg-white p-5">                   
              
                    <h2 class="mt-3 mb-3">In-House Training</h2>
                    <p class="mb-4">Non illo quas blanditiis repellendus laboriosam minima animi. Consectetur accusantium pariatur repudiandae!</p>
              
                    <a href="blog-single.html" class="btn btn-small btn-main btn-round-full">Learn More</a>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-3 col-md-6 mb-5">
                <div class="blog-item">
                  <img src="{{asset('assets/images/blog/2.jpg')}}" alt="" class="img-fluid rounded">
              
                  <div class="blog-item-content bg-white p-5">
                    
              
                    <h3 class="mt-3 mb-3">Blended Learning</h3>
                    <p class="mb-4">Non illo quas blanditiis repellendus laboriosam minima animi. Consectetur accusantium pariatur repudiandae!</p>
              
                    <a href="blog-single.html" class="btn btn-small btn-main btn-round-full">Learn More</a>
                  </div>
                </div>
              </div>

              <div class="col-lg-3 col-md-6 mb-5">
                <div class="blog-item">
                  <img src="{{asset('assets/images/blog/1.jpg')}}" alt="" class="img-fluid rounded">
              
                  <div class="blog-item-content bg-white p-5">
                   
              
                    <h3 class="mt-3 mb-3">E-Learning</h3>
                    <p class="mb-4">Non illo quas blanditiis repellendus laboriosam minima animi. Consectetur accusantium pariatur repudiandae!</p>
              
                    <a href="blog-single.html" class="btn btn-small btn-main btn-round-full">Learn More</a>
                  </div>
                </div>
              </div>
              
              <div class="col-lg-3 col-md-6 mb-5">
                <div class="blog-item">
                  <img src="{{asset('assets/images/blog/2.jpg')}}" alt="" class="img-fluid rounded">
              
                  <div class="blog-item-content bg-white p-5">
              
                    <h3 class="mt-3 mb-3">Custum content</h3>
                    <p class="mb-4">Non illo quas blanditiis repellendus laboriosam minima animi. Consectetur accusantium pariatur repudiandae!</p>
              
                    <a href="blog-single.html" class="btn btn-small btn-main btn-round-full">Learn More</a>
                  </div>
                </div>
              </div>
             
             
          </div>
         
      </div>
  </section>

  <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <section class="about-section spad color">
     <div class="container">
          
          <div class="row">
              
              <div class="col-lg-12">
                  
                      <div class="justify-content-center align-items-center text-margin1">
                           
                           <p class="f-para">There are several ways people can make money online. From selling products to advertising. In this article I am going to explain the concept of contextual advertising.
                            First I will explain what contextual advertising is. Contextual advertising means the advertising of products on a website according to the content the page is displaying. For example if the content of a website was information on a Ford truck then the advertisements would be for Ford trucks for sale, or Ford servicing etc. It picks up the words on the page and displays ads that are similar to those words. Then when someone either performs an action or clicks on your page you will get paid.</p>
                      </div>

                    

                    
              </div>

              
          </div>
      </div>
  </section>
    <br>
    <br>
    <br>
    <br>
    <br>
   
    <section class="about-section spad">
      <div class="container">
          
          <div class="row">
              
              <div class="col-lg-6">
                  <div class="about-text">
                      <h3>Benefits for the <br>company</h3>
                      <ul>
                          <li><span class="icon_check"></span> Kook 2 Directory Add Url Free</li>
                          <li><span class="icon_check"></span> Write On Your Business Card</li>
                          <li><span class="icon_check"></span> Advertising Outdoors</li>
                          <li><span class="icon_check"></span> Effective Advertising Pointers</li>
                          <li><span class="icon_check"></span> Kook 2 Directory Add Url Free</li>
                          <li><span class="icon_check"></span> Kook 2 Directory Add Url Free</li>
                          <li><span class="icon_check"></span> Kook 2 Directory Add Url Free</li>
                      </ul>
                  </div>
              </div>
              <div class="col-lg-6">
                <div class="about-pic">
                    <img src="{{asset('assets/img/about-us.jpg')}}" alt="">
                </div>
            </div>
          </div>
      </div>
  </section>
   
    



    <!-- Footer Section Begin -->
    <footer class="footer-section colorfooter">
        <div class="container">
            
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="widget">
                        <h4 class="text-capitalize mb-4 text-white">Company</h4>
    
                        <ul class="list-unstyled footer-menu lh-35">
                            <li class="text-white"><a href="#">Terms & Conditions</a></li>
                            <li class="text-white"><a href="#">Privacy Policy</a></li>
                            <li class="text-white"><a href="#">Support</a></li>
                            <li class="text-white"><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widget">
                        <h4 class="text-capitalize mb-4 textColor">Quick Links</h4>
    
                        <ul class="list-unstyled footer-menu lh-35">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
               
    
               
            </div>
            
            <div class="footer-btm pt-4">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                           <div class="copyright">
                            <!-- &copy; Copyright Reserved to <span class="text-color">Megakit.</span> by <a href="https://themefisher.com/" target="_blank">Themefisher</a> -->
                            <p class="text-white">  Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with</p>
                           
                               <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                    </div>
    
                    
                    <div class="col-lg-4 col-md-12 col-sm-12 text-left text-lg-left">
                        <ul class="list-inline footer-socials">
                            <li class="list-inline-item"><a href="https://www.facebook.com/themefisher"><i class="ti-facebook mr-2"></i>Facebook</a></li>
                            <li class="list-inline-item"><a href="https://twitter.com/themefisher"><i class="ti-twitter mr-2"></i>Twitter</a></li>
                            <li class="list-inline-item"><a href="https://www.pinterest.com/themefisher/"><i class="ti-linkedin mr-2 "></i>Linkedin</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>