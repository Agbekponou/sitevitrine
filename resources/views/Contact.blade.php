<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Test">
    <meta name="keywords" content="Manup, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact-us</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">


    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/plugins/themify/css/themify-icons.css')}}">
    <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
 
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container">
            <div class="logo">
                <a href="./index.html">
                   MON SITE
                </a>
            </div>
            <div class="nav-menu">
                <nav class="mainmenu mobile-menu">
                    <ul>
                        <li class=""><a href="/">Home</a></li>
                        <li class=""><a href="Labrary">Labrary</a></li>
                         <li class=""><a href="Corporate">Corporate</a></l>
                        <li class="active"><a href="Contact-us">Contact Us</a></li>
                        
                    </ul>
                </nav>
                
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Hero Section Begin -->
    <section class="hero-section set-bg" data-setbg="{{asset('assets/img/hero.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hero-text ">
                        <h2 style="text-align: center; padding-bottom: 200px;">Contact Us<br /> </h2>
                       <!-- <a href="#" class="primary-btn">Buy Ticket</a>-->
                    </div>
                </div>
               
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Counter Section Begin -->
    
    <!-- Counter Section End -->

    <!-- Home About Section Begin -->
    <section id="contact" class="section-bg">

        <div class="container" data-aos="fade-up">
  
          <div class="section-header">
            <h2>Contact Us</h2>
            <p>Nihil officia ut sint molestiae tenetur.</p>
          </div>
  
          <div class="row contact-info">
  
            <div class="col-md-4">
              <div class="contact-address">
                <i class="bi bi-geo-alt"></i>
                <h3>Address</h3>
                <address>A108 Adam Street, NY 535022, USA</address>
              </div>
            </div>
  
            <div class="col-md-4">
              <div class="contact-phone">
                <i class="bi bi-phone"></i>
                <h3>Phone Number</h3>
                <p><a href="tel:+155895548855">+1 5589 55488 55</a></p>
              </div>
            </div>
  
            <div class="col-md-4">
              <div class="contact-email">
                <i class="bi bi-envelope"></i>
                <h3>Email</h3>
                <p><a href="mailto:info@example.com">info@example.com</a></p>
              </div>
            </div>
  
          </div>
  
          <div class="form">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="form-group col-md-6">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                </div>
                <div class="form-group col-md-6 mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                </div>
              </div>
             
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>
  
        </div>
      </section>

   
    



    <!-- Footer Section Begin -->
    <footer class="footer-section colorfooter">
        <div class="container">
            
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="widget">
                        <h4 class="text-capitalize mb-4 textColor">Company</h4>
    
                        <ul class="list-unstyled footer-menu lh-35">
                            <li class="text-white"><a href="#">Terms & Conditions</a></li>
                            <li class="text-white"><a href="#">Privacy Policy</a></li>
                            <li class="text-white"><a href="#">Support</a></li>
                            <li class="text-white"><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widget">
                        <h4 class="text-capitalize mb-4 textColor">Quick Links</h4>
    
                        <ul class="list-unstyled footer-menu lh-35">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
             <!--    <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="widget">
                        <h4 class="text-capitalize mb-4">Subscribe Us</h4>
                        <p>Subscribe to get latest news article and resources  </p>
    
                        <form action="#" class="sub-form">
                            <input type="text" class="form-control mb-3" placeholder="Subscribe Now ...">
                            <a href="#" class="btn btn-main btn-small">subscribe</a>
                        </form>
                    </div>
                </div> -->
    
              <!--   <div class="col-lg-3 ml-auto col-sm-6">
                    <div class="widget">
                        <div class="logo mb-4">
                            <h3>Mega<span>kit.</span></h3>
                        </div>
                        <h6><a href="tel:+23-345-67890" >Support@megakit.com</a></h6>
                        <a href="mailto:support@gmail.com"><span class="text-color h4">+23-456-6588</span></a>
                    </div>
                </div> -->
            </div>
            
            <div class="footer-btm pt-4">
                <div class="row">
                    <div class="copyright">
                            <!-- &copy; Copyright Reserved to <span class="text-color">Megakit.</span> by <a href="https://themefisher.com/" target="_blank">Themefisher</a> -->
                            Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with
                               <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
    
                    <!-- <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="copyright">
                        Distributed by  <a href="https://themewagon.com/" target="_blank">Themewagon</a>
                        </div>
                    </div> -->
                    <div class="col-lg-4 col-md-12 col-sm-12 text-left text-lg-left">
                        <ul class="list-inline footer-socials">
                            <li class="list-inline-item"><a href="https://www.facebook.com/themefisher"><i class="ti-facebook mr-2"></i>Facebook</a></li>
                            <li class="list-inline-item"><a href="https://twitter.com/themefisher"><i class="ti-twitter mr-2"></i>Twitter</a></li>
                            <li class="list-inline-item"><a href="https://www.pinterest.com/themefisher/"><i class="ti-linkedin mr-2 "></i>Linkedin</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>