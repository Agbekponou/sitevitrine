<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Test">
    <meta name="keywords" content="Manup, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CAREER ACADEMY INSTITUTE (CAI) </title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600,700,800,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">


    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/plugins/themify/css/themify-icons.css')}}">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header-section">
        <div class="container">
            <div class="logo">
                <a href="./index.html">
                 Mon site
                </a>
            </div>
            <div class="nav-menu">
                <nav class="mainmenu mobile-menu">
                    <ul>
                        <li class="active"><a href="/">Home</a></li>
                        <li class=""><a href="Labrary">Labrary</a></li>
                        <li class=""><a href="Corporate">Corporate</a></li>
                        <li class=""><a href="Contact-us">Contact Us</a></li>
                    </ul>
                </nav>
                
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Hero Section Begin -->
    <section class="hero-section set-bg" data-setbg="{{asset('assets/img/hero.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="hero-text">
                        <span>For over 19 years and with over 5,000+ organized conferences, courses, in-house courses, e-learning courses, we continue to lead the field for the world’s top business professionals who want the knowledge, partnerships, and market insights needed for further growth.</span>
                        <h2>CAREER ACADEMY INSTITUTE (CAI) <br /> </h2>
                       <!-- <a href="#" class="primary-btn">Buy Ticket</a>-->
                    </div>
                </div>
                <div class="col-lg-5">
                    <img src="{{asset('assets/img/hero-right.png')}}" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->

    <!-- Counter Section Begin -->
    
    <!-- Counter Section End -->

    <!-- Home About Section Begin -->
    <section class="home-about-section spad">
        <div class="container">
            <div class="row ">
                
                <div class="col-lg-9">
                <div class="row text-left">
                <div class="col-lg-12 text-left">
                    <div class="section-title">
                        <span class="h6 text-color azer10">Events Comming</span>
                        <h2 class="mt-3 content-title  azer ">Event</h2>
                    </div>
                </div>
                    </div>
                    <div class="rounded">
                        <div class="row align-items-self">
                            
                          <div class="col-md-6 col-lg-5 mb-3 mb-lg-0">
                            <a href="single.html" class="d-flex post-sm-entry">
                              <figure class="mr-3 mb-0"><img src="{{asset('assets/img/team-member/member-1.jpg')}}" alt="Image" class="rounded"><span class="post-category  text-center m-0 mb-2">Online live|Training courses|Online courses</span></figure>
                              <div>
                                <span class="post-category bg-danger text-white m-0 mb-2">Travel</span>
                                <span class="post-category bg-danger text-white m-0 mb-2">19-21 Avril 2022</span>
                                
                                <h2 class="mb-0">The 20 Biggest Fintech Companies In America 2019</h2>
                          
                              </div>
                            </a>
                          </div>
          
                          <div class="col-md-6 col-lg-5 mb-3 mb-lg-0">
                            <a href="single.html" class="d-flex post-sm-entry">
                              <figure class="mr-3 mb-0"><img src="{{asset('assets/img/team-member/member-1.jpg')}}" alt="Image" class="rounded"><span class="post-category  text-center m-0 mb-2">Online live|Training courses|Online courses</span></figure>
                              <div>
                                <span class="post-category bg-warning text-white m-0 mb-2">Lifestyle</span>
                                <span class="post-category bg-danger text-white m-0 mb-2">19-21 Avril 2022</span>

                                <h2 class="mb-0">The 20 Biggest Fintech Companies In America 2019</h2>
                              </div>
                            </a>
                          </div>
          
                         <!--  <div class="col-md-6 col-lg-5 mb-3 mb-lg-0">
                            <a href="single.html" class="d-flex post-sm-entry">
                              <figure class="mr-3 mb-0"><img src="{{asset('assets/img/team-member/member-1.jpg')}}" alt="Image" class="rounded"><span class="post-category  text-center m-0 mb-2">Online live|Training courses|Online courses</span></figure>
                              <div>
                                <span class="post-category bg-success text-white m-0 mb-2">Nature</span>
                                <span class="post-category bg-danger text-white m-0 mb-2">19-21 Avril 2022</span>

                                <h2 class="mb-0">The 20 Biggest Fintech Companies In America 2019</h2>
                              </div>
                            </a>
                          </div> -->

                        <!--   <div class="col-md-6 col-lg-5 mb-3 mb-lg-0">
                            <a href="single.html" class="d-flex post-sm-entry">
                              <figure class="mr-3 mb-0"><img src="{{asset('assets/img/team-member/member-1.jpg')}}" alt="Image" class="rounded"><span class="post-category  text-center m-0 mb-2">Online live|Training courses|Online courses</span></figure>
                              <div>
                                <span class="post-category bg-success text-white m-0 mb-2">Nature</span>
                                <span class="post-category bg-danger text-white m-0 mb-2">19-21 Avril 2022</span>

                                <h2 class="mb-0">The 20 Biggest Fintech Companies In America 2019</h2>
                              </div>
                            </a>
                          </div> -->

                          
                          
                          
                        </div>
                        <div class="ha-text">
                            <a href="#" class="ha-btn">Voir nos evenements</a>                            
                        </div>        
                        
                      </div>
                </div>
                <div class="col-lg-3">
                    <div class="ha-text border p-4 color">
                        <div class="ha-text">
                            <h4>A Propos</h4>
                            <p>CAREER ACADEMY INSTITUTE (CAI) est votre partenaire en matière d'intelligence économique et votre société de gestion d'événements pour l'Afrique subsaharienne. </p>
                            
                            <a href="#" class="ha-btn">En savoir plus</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section counter color">
        <div class="container ">
            <div class="row">
                <!-- <div class="col-lg-12 text-left">
                    <div class="section-title">
                        <span class="h6 text-color">Specialized events</span>
                        <h2 class="mt-3 content-title ">Industries</h2>
                    </div>
                </div> -->
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="counter-item text-center mb-5 mb-lg-0">
                        <h3 class="mb-0"><span class="counter-stat font-weight-bold">19</span> </h3>
                        <p class="text-muted">Year of connections</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="counter-item text-center mb-5 mb-lg-0">
                        <h3 class="mb-0"><span class="counter-stat font-weight-bold">5,000 </span>+ </h3>
                        <p class="text-muted">Organized Events</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="counter-item text-center mb-5 mb-lg-0">
                        <h3 class="mb-0"><span class="counter-stat font-weight-bold">50,000</span>+</h3>
                        <p class="text-muted">Satisfied compagny</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- Home About Section End -->
     
    <section class="section service border-top">
        <div class="container">
            <div class="row text-left">
                <div class="col-lg-12 text-left">
                    <div class="section-title">
                        <span class="h6 text-color azer0">Specialized events</span>
                        <h2 class="mt-3 content-title azer1 ">Industries</h2>
                    </div>
                </div>
                <div class="col-lg-12 text-right">
                    <div class="section-title">
                        <span class="h6 text-color">See all events</span>
                        
                    </div>
                </div>
            </div>
            
            
    
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="service-item mb-5">
                       <a href=""><i class="ti-desktop icon"></i> </a> 
                        <h4 class="mb-3">Pharma</h4>
                    </div>
                </div>
    
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="service-item mb-5">
                        <i class="ti-layers icon"></i>
                        <h4 class="mb-3">Finance</h4>
                    </div>
                </div>
    
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="service-item mb-5">
                        <i class="ti-bar-chart icon"></i>
                        <h4 class="mb-3">Energy</h4>
                    </div>
                </div>
    
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="service-item mb-5 mb-lg-0">
                       <a href="">  <i class="ti-vector icon"></i></a>
                        <h4 class="mb-3">Logistic & Transport.</h4>
                    </div>
                </div>
    
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="service-item mb-5 mb-lg-0">
                        <i class="ti-android icon"></i>
                        <h4 class="mb-3">Chemical</h4>
                    </div>
                </div>
    
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="service-item mb-5 mb-lg-0">
                        <i class="ti-pencil-alt icon"></i>
                        <h4 class="mb-3">Oil & Gas</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <br>
    <br>
    <section class="section service border-top color">
        <div class="container">
            <div class="row text-left">
                <div class="col-lg-12 text-left">
                    <div class="section-title">
                        <span class="h6 text-color">Our portfolio</span>
                        <h2 class="mt-3 content-title ">Event types</h2>
                    </div>
                </div>
               <!--  <div class="col-lg-12 text-right">
                    <div class="section-title">
                        <span class="h6 text-color">See all events</span>
                        
                    </div>
                </div> -->
            </div>
            <div class="row ">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('assets/img/team-member/member-1.jpg')}}" class="card-img-top" alt="img/team-member/member-1.jpg">
                        <div class="card-body">
                          <h4 class="card-title"><strong>Training Courses</strong></h4>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary stretched-link">Go somewhere</a>
                        </div>
                      </div>
                </div>
    
                <div class="col-lg-3 col-md-6 col-sm-8">
                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('assets/img/team-member/member-1.jpg')}}" class="card-img-top" alt="img/team-member/member-1.jpg">
                        <div class="card-body">
                          <h4 class="card-title"><strong>Online conferences</strong></h4>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary stretched-link">Go somewhere</a>
                        </div>
                      </div>
                </div>
    
                <div class="col-lg-3 col-md-6 col-sm-8">
                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('assets/img/team-member/member-1.jpg')}}" class="card-img-top" alt="img/team-member/member-1.jpg">
                        <div class="card-body">
                          <h4 class="card-title"><strong>Conferences</strong></h4>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary stretched-link">Go somewhere</a>
                        </div>
                      </div>
                </div>
    
                <div class="col-lg-3 col-md-6 col-sm-8">
                    <div class="card" style="width: 18rem;">
                        <img src="{{asset('assets/img/team-member/member-1.jpg')}}" class="card-img-top" alt="img/team-member/member-1.jpg">
                        <div class="card-body">
                          <h4 class="card-title"><strong>Corporate Learning</strong></h4>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary stretched-link">Go somewhere</a>
                        </div>
                      </div>
                </div>
    
                
            </div>
        </div>
    </section>
    



<section class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-left">
                    <div class="section-title">
                        <h2 class="mt-3 content-title ">Nos Partennaires</h2>
                    </div>
                </div>
        </div>

  <div class="row">
            <p><strong>Nos partenaires événementiels</strong></p>
        </div>

        <div class="partner-logo owl-carousel">

          
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image1.jpg')}}" alt="">
                </div>
            </a>

            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image2.jpg')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image10.gif')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image3.jpg')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image4.jpg')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image5.png')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image6.jpg')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image7.jpg')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image8.jpg')}}" alt="">
                </div>
            </a>
            <a href="#" class="pl-table">
                <div class="pl-tablecell">
                    <img src="{{asset('assets/img/partner-logo/image9.jpg')}}" alt="">
                </div>
            </a>
        </div>
    </div>
</section>
</br>
</br>
</br>
    <!-- Team Member Section Begin -->

    <!-- Team Member Section End -->

    <!-- Schedule Section End -->

    <!-- Pricing Section End -->

    <!-- latest BLog Section Begin -->
    <section class="latest-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Library</h2>
                        <p>Do not miss anything topic abput the event</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="latest-item set-bg large-item" data-setbg="{{asset('assets/img/blog/latest-b/latest-1.jpg')}}">
                        <div class="li-tag">Marketing</div>
                        <div class="li-text">
                            <h4><a href="./blog-details.html">Improve You Business Cards And Enchan Your Sales</a></h4>
                            <span><i class="fa fa-clock-o"></i> 19th May, 2019</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="latest-item set-bg" data-setbg="{{asset('assets/img/blog/latest-b/latest-2.jpg')}}">
                        <div class="li-tag">Experience</div>
                        <div class="li-text">
                            <h5><a href="./blog-details.html">All users on MySpace will know that there are millions of people out there.</a></h5>
                            <span><i class="fa fa-clock-o"></i> 19th May, 2019</span>
                        </div>
                    </div>
                    <div class="latest-item set-bg" data-setbg="{{asset('assets/img/blog/latest-b/latest-3.jpg')}}">
                        <div class="li-tag">Marketing</div>
                        <div class="li-text">
                            <h5><a href="./blog-details.html">A Pocket PC is a handheld computer, which features many of the same capabilities.</a></h5>
                            <span><i class="fa fa-clock-o"></i> 19th May, 2019</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- latest BLog Section End -->
    
    <!-- Contact Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section colorfooter">
        <div class="container">
            
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="widget">
                        <h4 class="text-capitalize mb-4 textColor">Company</h4>
    
                        <ul class="list-unstyled footer-menu lh-35">
                            <li class="text-white"><a href="#">Terms & Conditions</a></li>
                            <li class="text-white"><a href="#">Privacy Policy</a></li>
                            <li class="text-white"><a href="#">Support</a></li>
                            <li class="text-white"><a href="#">FAQ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widget">
                        <h4 class="text-capitalize mb-4 textColor">Quick Links</h4>
    
                        <ul class="list-unstyled footer-menu lh-35 textColor">
                            <li class="textColor"><a href="#">About</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                  <!--   <div class="widget">
                        <h4 class="text-capitalize mb-4 textColor">Subscribe Us</h4>
                        <p class="textColor">Subscribe to get latest news article and resources  </p>
    
                        <form action="#" class="sub-form">
                            <input type="text" class="form-control mb-3" placeholder="Subscribe Now ...">
                            <a href="#" class="btn btn-main btn-small">subscribe</a>
                        </form>
                    </div> -->
                </div>
    
                <div class="col-lg-3 ml-auto col-sm-6">
                    <div class="widget">
                        <!-- <div class="logo mb-4">
                            <h3 class="textColor"><span>kit.</span></h3>
                        </div> -->
                        <!-- <h6><a href="tel:+23-345-67890" class="textColor" >Support@megakit.com</a></h6>
                        <a href="mailto:support@gmail.com"><span class="textcolor">+23-456-6588</span></a> -->
                    </div>
                </div>
            </div>
            
            <div class="footer-btm pt-4">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="copyright">
                            <!-- &copy; Copyright Reserved to <span class="text-color">Megakit.</span> by <a href="https://themefisher.com/" target="_blank">Themefisher</a> -->
                            Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with
                               <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                    </div>
    
                    <div class="col-lg-4 col-md-12 col-sm-12">
                       <!--  <div class="copyright">
                        Distributed by  <a href="https://themewagon.com/" target="_blank">Themewagon</a>
                        </div> -->
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 text-left text-lg-left">
                        <ul class="list-inline footer-socials">
                            <li class="list-inline-item"><a href="https://www.facebook.com/themefisher"><i class="ti-facebook mr-2" class="textColor"></i>Facebook</a></li>
                            <li class="list-inline-item"><a href="https://twitter.com/themefisher"><i class="ti-twitter mr-2"></i>Twitter</a></li>
                            <li class="list-inline-item"><a href="https://www.pinterest.com/themefisher/"><i class="ti-linkedin mr-2 "></i>Linkedin</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{asset('assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>